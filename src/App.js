import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import Navbar from "./components/Navbar/MainNavbar.js";

function App() {
  return (
    <div className="App">
       <Router>
        <Switch>
      <Route path="/main-navbar">
        <Navbar />
      </Route>
     
        </Switch>
        </Router>
    </div>
  );
}

export default App;
