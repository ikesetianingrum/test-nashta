import BrowseImage from "./browse-image.png";
import IconBack from "./back.svg";
import DefaultImageCard from "./defaultCardImg.png";
import IconMenu from "./icon-menu.svg";
import AddModule from "./addmodule.png";

export {
    BrowseImage,
    IconBack,
    DefaultImageCard,
    IconMenu,
    AddModule,
};