import React from "react";
import { Container, Grid, Paper, Button } from "@material-ui/core";
import styles from "./AddEvent.module.css";
import {BrowseImage} from "../../assets";

export default function AddEvent() {
  const title = "React JS";
  return (
    <Container fluid className={styles.container}>
      <div>
        <h1 className={styles.title}>Add Event</h1>
        <img src={BrowseImage} alt = "pictBrowser" className={styles.browseImage} />
        <h2 className={styles.titleModule}>Title *</h2>
        <div className={styles.isiTitle}>
          <p>{title}</p>
        </div>
        <Grid item xs={6}>
          <Paper className={styles.paper}>
            <p>
              Location 
            </p>
          </Paper>
          <Button
            variant="contained"
            color="secondary"
            className={styles.buttonBrowse}
          >
            Browse For Image
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Paper className={styles.paper}>
            <p>
              Participan 
            </p>
          </Paper>
          </Grid>
          <Grid item xs={6}>
          <Paper className={styles.paper}>
            <p>
              Date 
            </p>
          </Paper>
          </Grid>
        <h2 className={styles.descritionModule}>Note *</h2>
        <div className={styles.descript}>
          <textarea
            className={styles.descript2}
            placeholder="Lorem ipsum dolor sit amet"
          ></textarea>
        </div>
        <Button
          variant="contained"
          color="secondary"
          className={styles.buttonCancel}
        >
          Cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={styles.buttonSubmit}
        >
          Submit
        </Button>
      </div>
    </Container>
  );
}
