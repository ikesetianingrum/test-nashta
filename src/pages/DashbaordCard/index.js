import React, { Fragment} from "react";
import { Grid} from "@material-ui/core";
import styles from "./DashbaordCard.module.css";
import Card from "../../components/Card";

function DashboardCard() {

  return (
    <Fragment>
    
        <Grid
          container
          spacing={0}
          className={styles.container}
          alignContent="center"
          justify="center"
          direction="column"
        >
          <Grid
            item
            style={{
              marginBottom: "50px",
              marginTop: "60px",
              marginLeft: "52px",
            }}
          >
            <h1>Dashboard Acara</h1>
          </Grid>
          <Grid
            item
            style={{
              display: "flex",
              flexDirection: "row",
              marginLeft: "52px",
              marginBottom: "50px",
            }}
          >
            <Grid item style={{ width: "333px" }}>
            </Grid>
            <Grid item className={styles.filterSort}>
              <Grid
                style={{
                  marginRight: "40px",
                }}
              >
               
              </Grid>
              <Grid style={{ width: "256px", marginRight: "60px" }}>
              
              </Grid>
            </Grid>
          </Grid>
          <Grid
            container
            item
            direction="row"
            style={{ margin: "0 56px 24px", width: "1448px" }}
          >
            <Card
              status="Verified"
              title="Resepsi Pernikahan"
              penulis="Iqbal Syarifudin Lutfie"
              gambar=""
              uri="/"
            />
            <Card
              status="In Progress"
              title="Aqiqah"
              penulis="Laksiminita Ayuningtyas"
              gambar=""
              uri="/"
            />
            <Card
              status="Completed"
              title="Resepsi Pernikahan"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="Verified"
              title="Akad Nikah"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="Verified"
              title="Akad Nikah"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="New"
              title="Lahiran"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="In Progress"
              title="7 bulanan"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="In Progress"
              title="Baby Shower "
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="Completed"
              title="Midodareni"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
            <Card
              status="Revise"
              title="Akad Nikah"
              penulis="Mahmud Md"
              gambar=""
              uri="/"
            />
          </Grid>
        </Grid>
     
    </Fragment>
  );
}

export default DashboardCard;
