import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import "./navbar.css";
import { withRouter } from "react-router-dom";

class MainNavbar extends Component {
   render() {
    return (
      <Navbar className="navbar-custom">
        <Nav.Item className="nav ml auto">
          <Nav className="title-main-navbar">Brand Navbar </Nav>
        </Nav.Item>
        <Nav.Item className="nav ml-auto">
          <Nav.Link className="navbar-add-even" >
            Add Event 
          </Nav.Link>
          <Nav.Link className="navbar-dashboard" >
            Dashboard
          </Nav.Link>
        </Nav.Item>
      </Navbar>
    );
  
  };
}

export default withRouter(MainNavbar);
