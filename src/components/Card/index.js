import React from "react";
import { Card, CardContent } from "@material-ui/core";
import styles from "./card.module.css";
import { DefaultImageCard, IconMenu } from "../../assets";

const index = (props) => {
  const getBackgroundColorStatus = (status) => {
    if (status === "Completed") {
      return "#0097E6";
    } else if (status === "In Progress") {
      return "#FF7A00";
    } else if (status === "Verified") {
      return "#05A502";
    } else if (status === "New") {
      return "#BF5AF2";
    } else if (status === "Revise") {
      return "#FF0000";
    }
  };

  const cardClick = (props) => {
    window.location.href = props.uri;
    console.log("Title : ", props.title);
  };
  return (
    <Card
      className={styles.card}
      elevation={0}
      square
      title={props.title}
      style={{ padding: 0 }}
      onClick={() => cardClick(props)}
    >
      <CardContent style={{ margin: "24px 27px", padding: 0 }}>
        <div className={styles.content}>
          <div className={styles.cardImg}>
            {props.gambar === "" ? (
              <img src={DefaultImageCard} alt="" />
            ) : (
              <img src={props.gambar} alt="" />
            )}
          </div>
          <div className={styles.menuIcon}>
            <img src={IconMenu} alt="menu-icon"></img>
          </div>
        </div>
        <span
          className={styles.status}
          style={{ backgroundColor: getBackgroundColorStatus(props.status) }}
        >
          {props.status}
        </span>
        <p>{props.title}</p>
        <hr />
        <span className={styles.penulis}>{props.penulis}</span>
      </CardContent>
    </Card>
  );
};

export default index;